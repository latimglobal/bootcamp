// const gulp = require("gulp");
// const sass = require("gulp-sass");
// const autoprefixer = require("gulp-autoprefixer");

// gulp.task("default", function() {
//   console.log("hello world!");
// });

// gulp.task("styles", function() {
//   gulp
//     .src("sass/**/*.scss")
//     .pipe(sass().on("error", sass.logError))
//     .pipe(
//       autoprefixer({
//         browsers: ["last 2 versions"]
//       })
//     )
//     .pipe(gulp.dest("./css"));
// });
const gulp = require("gulp");
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const browserSync = require('browser-sync').create();

gulp.task("default",function(){
	gulp.watch('sass/**/*.scss',['styles']);
	// gulp.watch('sass/**/*.css').on('change', browserSync.reload);
	gulp.watch("./*.html").on('change', browserSync.reload);
	browserSync.init({
		'server':'./'
	});
});

gulp.task('styles',function(){
	gulp.src('sass/**/*.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(autoprefixer({
			overrideBrowserslist: ['last 2 versions']
		}))
		// .pipe(autoprefixer())
		.pipe(gulp.dest('./css'))
		.pipe(browserSync.stream());
});