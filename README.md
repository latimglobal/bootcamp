# bootcamp

An advanced code practice repo containing code snippets from different online tutorials.

AUGUST - SEPTEMBER - OCTOBER
=============================
Angular -> Advanced Bootcamp	1 hour
Machine Learning				30 mins
Android Apps					2 hours

NOVEMBER - DECEMBER
===================
Android Game Development		1 hour
MEAN Stack						1 hour
Serverless Programming			1 hour